package main

// A simple example demonstrating the use of multiple text input components
// from the Bubbles component library.

import (
	"fmt"
	"log"
	"strings"

	"git.sr.ht/~x4d6165/tarotbuddy/deck"
	"github.com/charmbracelet/bubbles/cursor"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/muesli/reflow/indent"
	"github.com/muesli/reflow/wordwrap"
	"github.com/muesli/termenv"
	Term "golang.org/x/term"
)

var (
	focusedStyle        = lipgloss.NewStyle().Foreground(lipgloss.Color("205"))
	blurredStyle        = lipgloss.NewStyle().Foreground(lipgloss.Color("240"))
	cursorStyle         = focusedStyle.Copy()
	noStyle             = lipgloss.NewStyle()
	helpStyle           = blurredStyle.Copy()
	cursorModeHelpStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("244"))
	alignStyle          = lipgloss.NewStyle().Align(lipgloss.Center, lipgloss.Center)
	cardStyle           = lipgloss.NewStyle().Foreground(lipgloss.Color("38")).
				Align(lipgloss.Left, lipgloss.Center)
	startStyle = lipgloss.NewStyle().
			Foreground(lipgloss.Color("158"))
	descStyle = lipgloss.NewStyle().
			Foreground(lipgloss.Color("129")).
			Align(lipgloss.Left)
	focusedButton = focusedStyle.Copy().Render("[ Convene with the wires ]")
	blurredButton = fmt.Sprintf("[ %s ]", blurredStyle.Render("Convene with the wires"))
)
var (
	term   = termenv.EnvColorProfile()
	subtle = makeFgStyle("241")
	dot    = colorFg(" • ", "236")
)

type (
	frameMsg struct{}
)

// Color a string's foreground with the given value.
func colorFg(val, color string) string {
	return termenv.String(val).Foreground(term.Color(color)).String()
}

// Return a function that will colorize the foreground of a given string.
func makeFgStyle(color string) func(string) string {
	return termenv.Style{}.Foreground(term.Color(color)).Styled
}

type model struct {
	focusIndex int
	inputs     []textinput.Model
	cursorMode cursor.Mode
	card       deck.Cards
	Choice     int
	Chosen     bool
	w, h       int
	reversed   bool
}

func initialModel() model {
	m := model{
		inputs:   make([]textinput.Model, 1),
		reversed: false,
	}
	m.w, m.h, _ = Term.GetSize(0)
	var t textinput.Model
	for i := range m.inputs {
		t = textinput.New()
		t.Cursor.Style = cursorStyle
		t.CharLimit = 130

		switch i {
		case 0:
			t.Placeholder = "What is your question for the wires?"
			t.Focus()
			t.PromptStyle = focusedStyle
			t.TextStyle = focusedStyle
		}

		m.inputs[i] = t
	}
	return m
}

func (m model) Init() tea.Cmd {
	return textinput.Blink
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	// Make sure these keys always quit
	if msg, ok := msg.(tea.KeyMsg); ok {
		k := msg.String()
		if k == "q" || k == "esc" || k == "ctrl+c" {
			return m, tea.Quit
		}
	}

	// Hand off the message and model to the appropriate update function for the
	// appropriate view based on the current state.
	if !m.Chosen {
		return updateChoices(msg, m)
	}
	return ReadingUpdate(msg, m)
}

func updateChoices(msg tea.Msg, m model) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "j", "down":
			m.Choice++
			if m.Choice > 2 {
				m.Choice = 2
			}
		case "k", "up":
			m.Choice--
			if m.Choice < 0 {
				m.Choice = 0
			}
		case "enter":
			m.Chosen = true
			return m, nil
		}
	}
	return m, nil
}

func checkbox(label string, checked bool) string {
	if checked {
		return colorFg("🕯️ "+label, "212")
	}
	return fmt.Sprintf("🕯️ %s", label)
}

func choicesView(m model) string {
	c := m.Choice

	tpl := `┌┬┐┌─┐┬─┐┌─┐┌┬┐┌┐ ┬ ┬┌┬┐┌┬┐┬ ┬
 │ ├─┤├┬┘│ │ │ ├┴┐│ │ ││ ││└┬┘
 ┴ ┴ ┴┴└─└─┘ ┴ └─┘└─┘─┴┘─┴┘ ┴ `
	tpl += "\n"
	tpl += "Welcome to TarotBuddy, your personal cyber-divination companion\n\n"
	tpl += "%s\n\n"
	tpl += subtle("j/k, up/down: select") + dot + subtle("enter: choose") + dot + subtle("q, esc: quit")

	choices := fmt.Sprintf(
		"%s\n",
		checkbox("Take a reading", c == 0),
	)

	return fmt.Sprintf(startStyle.Render(tpl), choices)
}

func ReadingUpdate(msg tea.Msg, m model) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.w = msg.Width
		m.h = msg.Height
		return m, nil
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "esc":
			return m, tea.Quit

		// Change cursor mode
		case "ctrl+r":
			m.cursorMode++
			if m.cursorMode > cursor.CursorHide {
				m.cursorMode = cursor.CursorBlink
			}
			cmds := make([]tea.Cmd, len(m.inputs))
			for i := range m.inputs {
				cmds[i] = m.inputs[i].Cursor.SetMode(m.cursorMode)
			}
			return m, tea.Batch(cmds...)

		// Set focus to next input
		case "tab", "shift+tab", "enter", "up", "down":
			s := msg.String()

			// Did the user press enter while the submit button was focused?
			// pick a card then
			if s == "enter" && m.focusIndex == len(m.inputs) {
				m.card = deck.PickCard(m.inputs[0].View())
				m.reversed = deck.IsReversed(m.inputs[0].View())
			}

			// Cycle indexes
			if s == "up" || s == "shift+tab" {
				m.focusIndex--
			} else {
				m.focusIndex++
			}

			if m.focusIndex > len(m.inputs) {
				m.focusIndex = 0
			} else if m.focusIndex < 0 {
				m.focusIndex = len(m.inputs)
			}

			cmds := make([]tea.Cmd, len(m.inputs))
			for i := 0; i <= len(m.inputs)-1; i++ {
				if i == m.focusIndex {
					// Set focused state
					cmds[i] = m.inputs[i].Focus()
					m.inputs[i].PromptStyle = focusedStyle
					m.inputs[i].TextStyle = focusedStyle
					continue
				}
				// Remove focused state
				m.inputs[i].Blur()
				m.inputs[i].PromptStyle = noStyle
				m.inputs[i].TextStyle = noStyle
			}

			return m, tea.Batch(cmds...)
		}
	}

	// Handle character input and blinking
	var cmds []tea.Cmd
	cmd := m.updateInputs(msg)
	cmds = append(cmds, cmd)
	cmds = append(cmds, cmd)

	return m, cmd
}

func (m *model) updateInputs(msg tea.Msg) tea.Cmd {
	cmds := make([]tea.Cmd, len(m.inputs))

	// Only text inputs with Focus() set will respond, so it's safe to simply
	// update all of them here without any further logic.
	for i := range m.inputs {
		m.inputs[i], cmds[i] = m.inputs[i].Update(msg)
	}

	return tea.Batch(cmds...)
}

func (m model) View() string {
	var s string
	if !m.Chosen {
		s = choicesView(m)
	} else {
		s = ReadingView(m)
	}
	return indent.String("\n"+s+"\n\n", 2)
}

func ReadingView(m model) string {
	var b strings.Builder

	for i := range m.inputs {
		b.WriteString(m.inputs[i].View())
		if i < len(m.inputs)-1 {
			b.WriteRune('\n')
		}
	}

	button := &blurredButton
	if m.focusIndex == len(m.inputs) {
		button = &focusedButton
	}
	fmt.Fprintf(&b, "\n\n%s\n\n", *button)
	b.WriteString(helpStyle.Render("cursor mode is "))
	b.WriteString(cursorModeHelpStyle.Render(m.cursorMode.String()))
	b.WriteString(helpStyle.Render(" (ctrl+r to change style) (ctrl+c to quit)"))
	b.WriteString("\n ✨✨✨✨✨\n\n")
	top := b.String()
	b.Reset()

	if m.reversed {
		b.WriteString(cardStyle.Render("Your card is: " + deck.Make_Name(m.card) + " Reversed"))
		b.WriteString(cardStyle.Render(deck.GetAscii(deck.Make_Name(m.card), true)))
		reading := lipgloss.JoinHorizontal(lipgloss.Center,
			wordwrap.String(b.String(), m.w/2),
			descStyle.Render(wordwrap.String(m.card.Reverse, m.w/2)))
		return lipgloss.JoinVertical(lipgloss.Top, top, reading)
	} else {
		b.WriteString(cardStyle.Render("Your card is: " + deck.Make_Name(m.card)))
		b.WriteString(cardStyle.Render(deck.GetAscii(deck.Make_Name(m.card), false)))
		reading := lipgloss.JoinHorizontal(lipgloss.Center,
			wordwrap.String(b.String(), m.w/2),
			descStyle.Render(wordwrap.String(m.card.Meaning, m.w/2)))
		return lipgloss.JoinVertical(lipgloss.Top, top, reading)
	}
}

func main() {
	p := tea.NewProgram(initialModel(), tea.WithAltScreen())
	if _, err := p.Run(); err != nil {
		log.Fatal(err)
	}
}
