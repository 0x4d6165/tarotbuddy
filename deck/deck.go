package deck

import (
	"encoding/json"
	"fmt"
	"hash/fnv"
	"math/rand"
	"time"

	"github.com/observerly/dusk/pkg/dusk"
)

import _ "embed"

type Deck struct {
	Cards []Cards `json:"deck"`
}
type Cards struct {
	Suit    string `json:"suit"`
	Value   string `json:"value"`
	Ordinal string `json:"ordinal"`
	Pic     string `json:"pic"`
	Descr   string `json:"descr"`
	Meaning string `json:"meaning"`
	Reverse string `json:"reverse"`
}

type Ascii struct {
	Deck []AsciiDeck `json:"deck"`
}
type AsciiDeck struct {
	Name string `json:"name"`
	Card string `json:"card"`
}

func hash(s string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}

//go:embed ascii.json
var asciiJson []byte

func GetAscii(card string, reversed bool) string {
	var data *Ascii
	errJson := json.Unmarshal([]byte(asciiJson), &data)
	if errJson != nil {
		fmt.Printf("could not unmarshal json: %s\n", errJson)
	}
	for i, selCard := range data.Deck {
		if selCard.Name == card {
			if !reversed {
				return selCard.Card
			} else {
				return data.Deck[i+78].Card
			}
		}
	}
	return ""
}

func Make_Name(card Cards) string {
	if card.Suit == "Major Arcana" {
		return card.Value
	} else if card.Descr == "" && card.Suit == "" {
		return ""
	}
	return card.Value + " of " + card.Suit
}

//go:embed tarot.json
var tarotJson []byte

func PickCard(query string) Cards {
	var data *Deck
	errJson := json.Unmarshal([]byte(tarotJson), &data)
	if errJson != nil {
		fmt.Printf("could not unmarshal json: %s\n", errJson)
		return data.Cards[0]
	}
	var seed int64 = time.Now().UTC().UnixNano() + int64(hash(query)) + int64(dusk.GetLunarEclipticPosition(time.Now()).Latitude)
	rand.Seed(seed)
	return data.Cards[rand.Intn(len(data.Cards))]
}

func IsReversed(query string) bool {
	rand.Seed(time.Now().UTC().UnixNano() + int64(hash(query)))
	return rand.Intn(2) == 1
}
