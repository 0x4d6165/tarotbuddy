package deck

import (
	"regexp"
	"testing"
)

func FuzzPickCard(f *testing.F) {
	testcases := []string{"Hello, world", "", "!12345"}
	for _, tc := range testcases {
		f.Add(tc) // Use f.Add to provide a seed corpus
	}
	f.Fuzz(func(t *testing.T, orig string) {
		want := regexp.MustCompile(`^(The )?[A-Z][a-zA-Z'’-]*( of [A-Z][a-zA-Z'’-]*)?`)
		msg := PickCard(orig).Value
		if !want.MatchString(msg) {
			t.Fatalf(`PickCard(%s) = %s, want match for %s, nil`, orig, msg, want)
		}
	})
}

func TestGetAscii(t *testing.T) {
	name := "The Devil"
	want := regexp.MustCompile(`^\s*\.-------------------\.\n(\s*\|.*\|\n)+\s*\x60-------------------´\s*$`)
	msg := GetAscii(name)
	if !want.MatchString(msg) {
		t.Fatalf(`PickCard(%s) = %s, want match for %s, nil`, name, msg, want)
	}
}

func TestMake_Name(t *testing.T) {
	testCases := []struct {
		card     Cards
		expected string
	}{
		// Test case 1: Major Arcana card
		{
			card: Cards{
				Suit:  "Major Arcana",
				Value: "The Fool",
			},
			expected: "The Fool",
		},

		// Test case 2: Card with suit and value
		{
			card: Cards{
				Suit:  "Cups",
				Value: "Ace",
			},
			expected: "Ace of Cups",
		},

		// Test case 3: Card with empty description and empty suit
		{
			card: Cards{
				Suit:  "",
				Value: "",
				Descr: "",
			},
			expected: "",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.expected, func(t *testing.T) {
			result := Make_Name(tc.card)
			if result != tc.expected {
				t.Errorf("Expected %q, but got %q", tc.expected, result)
			}
		})
	}
}

func TestHash(t *testing.T) {
	testCases := []struct {
		input    string
		expected uint32
	}{
		// Test case 1: Empty string
		{
			input:    "",
			expected: 2166136261, // FNV-1a hash of an empty string
		},

		// Test case 2: Non-empty string
		{
			input:    "hello",
			expected: 1335831723, // FNV-1a hash of "hello"
		},
	}

	for _, tc := range testCases {
		t.Run(tc.input, func(t *testing.T) {
			result := hash(tc.input)
			if result != tc.expected {
				t.Errorf("Expected hash %d for input %q, but got %d", tc.expected, tc.input, result)
			}
		})
	}
}
