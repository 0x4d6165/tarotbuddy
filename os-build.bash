#!/usr/bin/env bash

# Define the module and version
MODULE="git.sr.ht/~x4d6165/tarotbuddy"
VERSION="v0.4.0"

# Output directory
OUTPUT_DIR="binaries"

# Create the output directory if it doesn't exist
mkdir -p "$OUTPUT_DIR"

# List of target platforms and architectures
PLATFORMS=("linux" "darwin" "windows" "freebsd" "openbsd" "netbsd")
ARCHITECTURES=("amd64" "386")

# Iterate through each platform and architecture
for PLATFORM in "${PLATFORMS[@]}"; do
  for ARCH in "${ARCHITECTURES[@]}"; do
    # Define the output filename based on the platform and architecture
    OUTPUT_NAME="$OUTPUT_DIR/tarotbuddy_${VERSION}_${PLATFORM}_${ARCH}"

    # Build the binary for the current platform and architecture
    GOOS="$PLATFORM" GOARCH="$ARCH" go build -o "$OUTPUT_NAME" "$MODULE"
    tar -czf "$OUTPUT_NAME.tar.gz" "$OUTPUT_NAME"
    # Check if the build was successful
    if [ $? -eq 0 ]; then
      echo "Built: $OUTPUT_NAME"
    else
      echo "Build failed for: $PLATFORM $ARCH"
    fi
  done
done

