# tarotbuddy

tarotbuddy is a digital tarot card reading TUI application written in Go that actually uses your query in the random number generatrion.

## Building

``` shell
go get .
go build
```
or

``` shell
nix build
```
using the included flake

## Installing

``` shell
go install git.sr.ht/~x4d6165/tarotbuddy@latest
```
or use one of the prebuilt binaries for each release

## Licensing

Tarot card ascii art is [MIT Licensed Copyright (c) Kathryn Isabelle Lawrence 2020](https://github.com/lawreka/ascii-tarot/blob/master/LICENSE.txt)
